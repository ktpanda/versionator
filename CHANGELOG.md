[Version 0.0.6 (2025-02-10)](https://pypi.org/project/versionator/0.0.6/)
============================

* Don't try to run `pip install` if there are no dependencies to install ([e599403](https://gitlab.com/ktpanda/versionator/-/commit/e59940304d12d3b7e73a94a7dba3fa9d5aca1903))


[Version 0.0.5 (2022-09-27)](https://pypi.org/project/versionator/0.0.5/)
============================

* Add gendoc and README.html ([827ba7b](https://gitlab.com/ktpanda/versionator/-/commit/827ba7b659766a53be013dc187603d9be326c9b3))
* Include files with no extension when checking for version, so scripts are checked as well ([56e2862](https://gitlab.com/ktpanda/versionator/-/commit/56e28628bb3414def2ce4b8b5ca6ce8cf78f2437))


[Version 0.0.4 (2022-09-27)](https://pypi.org/project/versionator/0.0.4/)
============================

* Add --deps option to install package dependencies ([55a14a3](https://gitlab.com/ktpanda/versionator/-/commit/55a14a369bd4ce6dc182b8968c2e93983ef70989))


[Version 0.0.3 (2022-09-01)](https://pypi.org/project/versionator/0.0.3/)
============================

* Add --confirm-all and combine confirmation for commit and tag ([c5b2431](https://gitlab.com/ktpanda/versionator/-/commit/c5b243125e4fcf3347e20f530f11997d3886080e))
* Don't include merges in CHANGELOG.md ([31d4d53](https://gitlab.com/ktpanda/versionator/-/commit/31d4d53cab2e68ec617772f46f56cae0d5b8ce48))
* Fix bugs with --confirm ([c2f7b5c](https://gitlab.com/ktpanda/versionator/-/commit/c2f7b5c6fe44f05b06e479fe989bbe4724606ff9))


[Version 0.0.2 (2022-08-28)](https://pypi.org/project/versionator/0.0.2/)
============================

* Exclude virtualenv dirs when scanning for files to update version in ([de90878](https://gitlab.com/ktpanda/versionator/-/commit/de90878fb57ffb5f276239515dcbbbbc88c50f42))
* Small bug fixes ([7f08fdc](https://gitlab.com/ktpanda/versionator/-/commit/7f08fdcaaa703fb58ad2d416b06c6a38b1dd1eee))


[Version 0.0.1 (2022-08-28)](https://pypi.org/project/versionator/0.0.1/)
============================

* Initial release ([82fc6c6](https://gitlab.com/ktpanda/versionator/-/commit/82fc6c6d9b10c40e4d50fdec7b3fda5f4e177fe6))
