versionator
===========

A tool for managing Python packages:

* Increment or set the version number
* Auto-update changelog based on commit messages, with option to edit
* Make a version commit
* Tag the commit with the version
* Push to origin
* Build the package
* Install locally using pip
* Publish using twine

This is primarily made for my own projects, but it could be useful for others.
